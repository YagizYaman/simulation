#include <R.h>
#include <Rmath.h>
#include <stdio.h>
#include <math.h>


/*

This code is the C implementation of the replenishment simulation,
it is called from R and must be compiled to obtain a dll file to
open an interface to C. This may require installation of Rtools

https://cran.r-project.org/bin/windows/Rtools/

A compiler will be necessary to obtain dll files and Rtools have it.
While installing, make sure compiler (i.e mingw) related path information
is added to system path (will be asked during installation).

*****************************************************************

In command window, you can create dll (for Windows
operating systems), you can use:

R CMD SHLIB C:/tailored/SeparatingV1/closed_loop_sim_with_c8.c

So, the format is >- R - CMD - SHLIB - the path of the C file

After compilation, a dll will be created in the folder. You should have R folder defined
in environment variables or explicitly provide the R path to be able to run this code.

You can find R path by writing file.path(R.home("bin"), "R") to your
RStudio(or another compiler) console.

 For example:

C:/PROGRA~1\R\R-41~1.2\bin\x64\R.exe CMD SHLIB C:/tailored/closed_loop_sim_with_c8.c


Note that this dll is created for 64 bit system (x64 version of R is used). It is always
safer to create *.bat file and compile. Create a flat text file and enter the following
with your path information:

C:/R/R-3.2.2/bin/x64/R.exe CMD SHLIB simulateWith_C_v4.c
pause




save it as "run.bat". Make sure that the C code and bat file is in the same folder.
Double clicking "run.bat" will be enough for compiling. "pause" command will allow you
if there is any problem with the compilation

Below provides the column indices for the simulation output, the output table
is provided as an input from R with the following order:

  out <- .C("C_simulation",
            as.integer(nofDcProductComb),
            as.integer(simulationTimeInfoT$startPeriod),    - default 0
            as.integer(simulationTimeInfoT$duration),       - depends on combinations
            as.integer(inputData$leadtime),                 - default 2
            as.double(simulationInput),                    -  keeps Dayorder | Demand
            result=as.double(simulationOutput),            -  STARTSTOCK RECEIVABLES SIMSALES ENDSTOCK
            as.integer(nrow(simulationOutput))
            as.integer(ncol(simulationOutput))

In the matrix form;
0. "STARTSTOCK"
1. "RECEIVABLES"
2. "SIMRENTS"
3. "ENDSTOCK"
4. "ADDSTOCK"
5. "REMOVESTOCK"
6. "ATTRITION"

SimulationIn
0: DayOrder
1: demand
2: demand1
3: demand2


*/

void C_simulation(int *numberofSimRents, int *numberOfCombinations, int *simulationStart,  int *periodLength,
                    int *leadTime, int *simulationInput, int *simulationOutput, int *nofRowsOutput, int *nofColsOutput
                    , int *nofRowsInput, int *nofColsInput, int *nofRowsLead, int *nofColsLead) {


	int k;			    //index of each key
	int i; 			    //index of each period
	int nofrows;    	//number of rows in the input
	int nofcols;        //number of columns in the input
    int nofrowsIn;
    int nofcolsIn;
    int nofrowslead;
    int nofcolslead;
    int curperiodlen;   //current period length
    int simstart;
    int cumulativeIndex=0;
    int norentstartstock;
    int startstock;
    int receivables;
    int simrents;
    int endstock;
    int addstock;
    int removestock;
    int attrition;
    int startstockfor;
    int numberofsimrents;

    numberofsimrents = *numberofSimRents;

    int simrentals[numberofsimrents];

    for (int i = 0; i < numberofsimrents; i++) {
        simrentals[i] = i + 4;
    }

    norentstartstock=0;
    startstock=1;
    receivables=2;
    simrents=3;
    endstock=simrentals[numberofsimrents - 1] + 1;
    startstockfor=simrentals[numberofsimrents - 1] + 2;
    addstock=simrentals[numberofsimrents - 1] + 3;
    removestock=simrentals[numberofsimrents - 1] + 4;
    attrition=simrentals[numberofsimrents - 1] + 5;



    Rprintf("Total number of Store-SKU combinations: %d\n",*numberOfCombinations);

	// total number of periods in the simulation data
	// required for column indexing
    Rprintf("Check-1");

    nofrows=*nofRowsOutput;
    nofcols=*nofColsOutput;
    nofrowsIn =*nofRowsInput;
    nofcolsIn =*nofColsInput;
    nofrowslead =*nofRowsLead;
    nofcolslead =*nofColsLead;


    Rprintf("Check-2");
// Returning array simulationOutput to matrix simulationOut to perform operations in a matrix form.
    int simulationOut[nofrows][nofcols];
    int simulationIn[nofrowsIn][nofcolsIn];
    int leadtimes[nofrowslead][nofcolslead];

    int Indexout = 0;
    int Indexin = 0;
    int Indexlead = 0;

    for (int j=0; j < nofcols; j++) {
        for (int i=0; i < nofrows; i++) {
            simulationOut[i][j] = simulationOutput[Indexout];
            Indexout++;
    }
}

    for (int j=0; j < nofcolsIn; j++) {
        for (int i=0; i < nofrowsIn; i++) {
            simulationIn[i][j] = simulationInput[Indexin];
            Indexin++;
    }
}


    for (int j=0; j < nofcolslead; j++) {
        for (int i=0; i < nofrowslead; i++) {
            leadtimes[i][j] = leadTime[Indexlead];
            Indexlead++;
    }
}

    Rprintf("Check-3");
    Rprintf("%d\n",simulationOut[0][1]);  // for debugging




	// For each DC-Product combination
    for(k=0;k<*numberOfCombinations;k++){

		// length of the simulation period for DC-product combination indexed with i
		curperiodlen=periodLength[k];

		// define the simulation length for this DC-product combination
		simstart=simulationStart[k];
        // Rprintf("%d\n",i); //for debugging
        // Start simulation based on the earliest time + simstart (this is a delay for simulation start)
		// loop simulate each day and perform the described operations

        for(i=cumulativeIndex+simstart;i<cumulativeIndex+curperiodlen;i++){


         // Define StartStock for each day.
         // StartStock[d] = EndStock[d-1] + Receivables[d]
         // if SimStart is zero, simulation starts at the provided first day for this DC-product combination
         // However we do not know the EndStock of the previous period (it is not in the data)

            if( simstart==0 && i == (cumulativeIndex+simstart)) {

                // StartStock[d]= StartStock[d]+ Receivables[d] + AddStock[d] - RemoveStock[d] - Attrition[d];
                simulationOut[i][startstock] = simulationOut[i][startstock] + simulationOut[i][receivables] + simulationOut[i][addstock] - simulationOut[i][removestock]-simulationOut[i][attrition];
            }else {
                // (EndStock[d-1] + Receivables[d] + AddStock[d]  > RemoveStock[d]+Attrition[d])
                if (simulationOut[i-1][endstock] + simulationOut[i][receivables] + simulationOut[i][addstock]  > simulationOut[i][removestock]+simulationOut[i][attrition]){

                    // StartStock[d]= EndStock[d-1]+ Receivables[d] + AddStock[d]  - RemoveStock[d] - Attrition[d];
                    simulationOut[i][startstock] = simulationOut[i-1][endstock] + simulationOut[i][receivables] + simulationOut[i][addstock]  - simulationOut[i][removestock] -simulationOut[i][attrition] ;
                }
                else {
                    simulationOut[i][startstock] = simulationOut[i-1][endstock] + simulationOut[i][receivables] + simulationOut[i][addstock];
                }
            }


            // Define non-rented starstock excluding simrents. Only include addstock, removestock & attrition.
              if( simstart==0 && i == (cumulativeIndex+simstart)) {

                // norentstartstock[d]= norentstartstock[d]+ Receivables[d] + AddStock[d] - RemoveStock[d] - Attrition[d];
                simulationOut[i][norentstartstock] = simulationOut[i][norentstartstock] + simulationOut[i][receivables] + simulationOut[i][addstock] - simulationOut[i][removestock]-simulationOut[i][attrition];
            }else {
                // norentstartstock[d-1] + AddStock[d]  > RemoveStock[d]+Attrition[d])
                if (simulationOut[i-1][norentstartstock] + simulationOut[i][addstock]  > simulationOut[i][removestock]+simulationOut[i][attrition]){

                    // norentstartstock[d]= EndStock[d-1] + AddStock[d]  - RemoveStock[d] - Attrition[d];
                    simulationOut[i][norentstartstock] = simulationOut[i-1][norentstartstock] + simulationOut[i][addstock]  - simulationOut[i][removestock] -simulationOut[i][attrition] ;
                }
                else {
                    simulationOut[i][norentstartstock] = simulationOut[i-1][norentstartstock] + simulationOut[i][addstock];
                }
            }

            // Define startstockfor

            if( simstart==0 && i == (cumulativeIndex+simstart)) {

                // StartStock[d]= StartStock[d]+ Receivables[d] + AddStock[d] - RemoveStock[d] -Attrition[d];
                simulationOut[i][startstockfor] = simulationOut[i][startstockfor] + simulationOut[i][receivables] + simulationOut[i][addstock] - simulationOut[i][removestock]-simulationOut[i][attrition];
            }else {
                // (EndStock[d-1] + Receivables[d] + AddStock[d]  > RemoveStock[d]+Attrition[d])
                if (simulationOut[i-1][endstock] + simulationOut[i][receivables] + simulationOut[i][addstock]  > simulationOut[i][removestock]+simulationOut[i][attrition]){

                    // StartStock[d]= EndStock[d-1]+ Receivables[d] + AddStock[d]  - RemoveStock[d];
                    simulationOut[i][startstockfor] = simulationOut[i-1][endstock] + simulationOut[i][receivables] + simulationOut[i][addstock]  - simulationOut[i][removestock] -simulationOut[i][attrition] ;
                }
                else {
                    simulationOut[i][startstockfor] = simulationOut[i-1][endstock] + simulationOut[i][receivables] + simulationOut[i][addstock];
                }
            }


        // Perform SimRents.
        // if StartStock[d] < Demand[d], we can sell at most the amount in the stock -> SimRents[d] = StartStock[d]
		// Otherwise demand is covered -> SimRents[d] = Demand[d]


            for (int z = 0; z < nofcolslead; z++) {
                if (simulationOut[i][startstockfor] < simulationIn[i][2+z]) {                                        // We can sell at most the amount in the stock : StartStock[d] < Demand[d]
                    simulationOut[i][simrentals[z]] = simulationOut[i][startstockfor];                                       // SimRents[d] = StartStock[d];
                    simulationOut[i][startstockfor] = simulationOut[i][startstockfor] - simulationOut[i][simrentals[z]];
                }else {
                    simulationOut[i][simrentals[z]] = simulationIn[i][2+z] ;                                                 // SimRents[d] = Demand[d]
                    simulationOut[i][startstockfor] = simulationOut[i][startstockfor] - simulationOut[i][simrentals[z]];
                }
            }

        // Perform simrents.
            for (int y=0; y < numberofsimrents; y++){
                simulationOut[i][simrents] = simulationOut[i][simrents]+simulationOut[i][simrentals[y]];
            }



        // Define Receivables. We'll use the lead time variable.

            for (int z = 0; z < nofcolslead; z++){
                if(i+leadtimes[i][z] < cumulativeIndex+curperiodlen) {
                    simulationOut[i+leadtimes[i][z]][receivables] = simulationOut[i+leadtimes[i][z]][receivables] + simulationOut[i][simrentals[z]];         // Receivables[d+leadtime] = Receivables[d+leadtime]+SimRentals[d];
                }
            }


        // Update EndStock.
        // EndStock(t) = max ( 0,  StartStock[d] - SimRents[d] )

           simulationOut[i][endstock] = simulationOut[i][startstock] - simulationOut[i][simrents];      // EndStock[d] = StartStock[d] - SimRents[d];
           if (simulationOut[i][endstock]< 0) simulationOut[i][endstock] = 0;                    // (EndStock[d] < 0) EndStock[d] = 0 ;



        // update the row index so that we are at the row for the new DC-production combinations
        }
        cumulativeIndex=cumulativeIndex+curperiodlen;
    }

// Return matrix to array

    Rprintf("Check-4");

    int Indexlast=0;

    for (int j=0; j < nofcols; j++) {
        for (int i=0; i < nofrows; i++) {
            simulationOutput[Indexlast] = simulationOut[i][j];
            Indexlast++;
    }
}


}
