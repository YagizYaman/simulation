#include <Rcpp.h>
using namespace Rcpp;

// [[Rcpp::export]]
NumericMatrix cpp_Simulation( NumericMatrix SimulationTime, 
                              NumericMatrix leadtimes,
                              NumericMatrix simulationInput, 
                              NumericMatrix SimulationOutput){
  int k;			    //index of each key
  int i; 			    //index of each period
  int curperiodlen;   //current period length
  int simstart;
  int cumulativeIndex= 0;
  int numberofsimrents = leadtimes.ncol(); // number of different leadtimes
  
  NumericVector simrentals(numberofsimrents);
  for (int i = 0; i < numberofsimrents; i++) {
    simrentals[i] = i + 4;
  }
  int norentstartstock= 0;
  int startstock= 1;
  int receivables= 2;
  int simrents= 3;
  int endstock= simrentals[numberofsimrents - 1] + 1;
  int startstockfor= simrentals[numberofsimrents - 1] + 2;
  int addstock= simrentals[numberofsimrents - 1] + 3;
  int removestock= simrentals[numberofsimrents - 1] + 4;
  int attrition= simrentals[numberofsimrents - 1] + 5;
  
  // For each DC-Product combination
    for(k=0; k<SimulationTime.nrow(); k++){
  
      // Define StartStock for each day.
      // StartStock[d] = EndStock[d-1] + Receivables[d]
      // if SimStart is zero, simulation starts at the provided first day for this DC-product combination
      // However we do not know the EndStock of the previous period (it is not in the data)
      
      simstart= SimulationTime(k, 0);
      curperiodlen= SimulationTime(k, 1);
      
      // Start simulation based on the earliest time + simstart (this is a delay for simulation start)
      // loop simulate each day and perform the described operations      
  
      for(i=cumulativeIndex+simstart; i<cumulativeIndex+curperiodlen; i++){
        if( simstart==0 && i == (cumulativeIndex+simstart)) {
          SimulationOutput(i,startstock) += SimulationOutput(i,receivables) + SimulationOutput(i,addstock) - SimulationOutput(i,removestock)-SimulationOutput(i,attrition);
        }else {
          SimulationOutput(i,startstock) = SimulationOutput(i-1,endstock) + SimulationOutput(i,receivables) + SimulationOutput(i,addstock)  - SimulationOutput(i,removestock) -SimulationOutput(i,attrition) ;
        }
           
        // Define non-rented starstock excluding simrents. Only include addstock, removestock & attrition.
        if( simstart==0 && i == (cumulativeIndex+simstart)) {
            SimulationOutput(i,norentstartstock) += SimulationOutput(i,receivables) + SimulationOutput(i,addstock) - SimulationOutput(i,removestock)-SimulationOutput(i,attrition);
        }else {
            SimulationOutput(i,norentstartstock) = SimulationOutput(i-1,norentstartstock) + SimulationOutput(i,addstock)  - SimulationOutput(i,removestock) -SimulationOutput(i,attrition) ;
            }
        if (SimulationOutput(i,norentstartstock) < 0) SimulationOutput(i,norentstartstock) = 0;
  
        // Define startstockfor
        if( simstart==0 && i == (cumulativeIndex+ simstart)) {
            SimulationOutput(i,startstockfor) += SimulationOutput(i,receivables) + SimulationOutput(i,addstock) - SimulationOutput(i,removestock)-SimulationOutput(i,attrition);
        }else {
            SimulationOutput(i,startstockfor) = SimulationOutput(i-1,endstock) + SimulationOutput(i,receivables) + SimulationOutput(i,addstock)  - SimulationOutput(i,removestock) -SimulationOutput(i,attrition) ;
            }
        if (SimulationOutput(i,startstockfor) < 0) SimulationOutput(i,startstockfor) = 0;
  
         // Prepare simrentals.
         // if StartStock[d] < Demand[d], we can sell at most the amount in the stock -> SimRents[d] = StartStock[d]
         // Otherwise demand is covered -> SimRents[d] = Demand[d]
        for (int z = 0; z < leadtimes.ncol(); z++) {
            if (SimulationOutput(i,startstockfor) < simulationInput(i,z)) {               // We can sell at most the amount in the stock : StartStock[d] < Demand[d]
                SimulationOutput(i,simrentals[z]) = SimulationOutput(i,startstockfor);    // SimRents[d] = StartStock[d];
                SimulationOutput(i,startstockfor) -= SimulationOutput(i,simrentals[z]);
            }else {
                SimulationOutput(i,simrentals[z]) = simulationInput(i,z) ;                // SimRents[d] = Demand[d]
                SimulationOutput(i,startstockfor) -= SimulationOutput(i,simrentals[z]);
            }
        }
  
        // Perform simrentals.
        for (int y=0; y < numberofsimrents; y++){
            SimulationOutput(i,simrents) += SimulationOutput(i,simrentals[y]);
        }
  
        // Define receivables. 
        for (int z = 0; z < leadtimes.ncol(); z++){
            if(i+leadtimes(i,z) < cumulativeIndex+ curperiodlen) {
                SimulationOutput(i+leadtimes(i,z),receivables) += SimulationOutput(i,simrentals[z]);         
            }
        }
  
        // Update EndStock.
        // EndStock(t) = max ( 0,  StartStock[d] - SimRents[d] )
       SimulationOutput(i,endstock) = SimulationOutput(i,startstock) - SimulationOutput(i,simrents);      
       if (SimulationOutput(i,endstock)< 0) SimulationOutput(i,endstock) = 0;                   
  
    // update the row index so that we are at the row for the new DC-production combinations
    }
    cumulativeIndex=cumulativeIndex+curperiodlen;
  
    }
  
  return SimulationOutput;
                 }


