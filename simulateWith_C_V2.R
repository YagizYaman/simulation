sourceCpp(file.path(gitpath,"order_engine/CppFunction.cpp"))

simulateWith_C <- function(inputData,num_sim_rents) {
  
  inputData = inputData[order(HUB,STYLE_NO,SIZE_CODE,DATE)]
  inputData[,WeekOrder:=1:.N, list(HUB,STYLE_NO,SIZE_CODE)] 
  
  inputData[is.na(ON_HAND) | ON_HAND<0 ,ON_HAND:=0]
  
  sim_rents_cols <- paste0("SIMRENTS", c(1:num_sim_rents))
  lt_cols <- paste0("leadtime", c(1:num_sim_rents))
  demand_cols <- paste0("demand", c(1:num_sim_rents))
  
  inputData <- inputData[, c(
    "NORENT_STARTSTOCK",
    "STARTSTOCK", 
    "RECEIVABLES", 
    "SIMRENTS",
    sim_rents_cols,
    "ENDSTOCK",
    "STARTSTOCKFOR") := 0]
  
  inputData[WeekOrder == 1, RECEIVABLES := ON_HAND]
  inputData[WeekOrder == 14, ADD_STOCK := ADD_STOCK + ADD_RENTED]
  
  setkeyv(inputData,c('HUB','STYLE_NO', 'SIZE_CODE'))
  inputData = inputData[,.SD[order(WeekOrder)],by=key(inputData)]
  
  simulationTimeInfo = inputData[,list(startPeriod=0, 
                                       duration=length(WeekOrder)), 
                                 by=key(inputData)]
  
  simulationTimeInfo <- simulationTimeInfo[,.(startPeriod, duration)]
  
  # create matrices for C++ operations
  
  leadtimes <- as.matrix(inputData[,c(lt_cols),
                                   with=FALSE])
  
  simulationOutput=as.matrix(inputData[,c(
    "NORENT_STARTSTOCK",
    "STARTSTOCK", 
    "RECEIVABLES",
    "SIMRENTS",
    sim_rents_cols,
    "ENDSTOCK",
    "STARTSTOCKFOR",
    "ADD_STOCK",
    "REMOVE_STOCK",
    "ATTRITION"),
    with=FALSE])
  
  simulationInput=as.matrix(inputData[,c(demand_cols),with=FALSE])
  
  simulationTimeInfo <- as.matrix(simulationTimeInfo)
  
  # Calling the cpp function
  SimulationOutput <- cpp_Simulation(simulationTimeInfo, 
                                     leadtimes, 
                                     simulationInput, 
                                     simulationOutput)
  
  SimulationOutput <- as.data.table(SimulationOutput)
  
  output <- data.table(inputData[,1:(ncol(inputData)-11-length(sim_rents_cols))
                                 ,with=FALSE],SimulationOutput)
  
  output[,Unfulfilled_RESV := demand-SIMRENTS]
  
  for (j in 1:num_sim_rents){
    output[, paste0("Unfulfilled_RESV",j) :=
             (get(demand_cols[j]) - get(sim_rents_cols[j]))]
  }
  
  
  return(output)
}
